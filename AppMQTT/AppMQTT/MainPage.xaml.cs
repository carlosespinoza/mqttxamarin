﻿using OpenNETCF.MQTT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppMQTT
{
    public partial class MainPage : ContentPage
    {
        MQTTClient mqtt;
        List<string> mensajes;
        int m = 0;
        public MainPage()
        {
            InitializeComponent();
            mensajes = new List<string>();
            mqtt = new MQTTClient("broker.hivemq.com", 1883);
            mqtt.MessageReceived += Mqtt_MessageReceived;
            mqtt.Connect("AppMovilESP8266");
            mqtt.Subscriptions.Add(new Subscription("ServerProfeCarlos"));
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                if (mensajes.Count > m)
                {
                    lstMensajes.ItemsSource = null;
                    lstMensajes.ItemsSource = mensajes;
                    m = mensajes.Count;
                }
                return true;
            });
        }

        private void Mqtt_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            mensajes.Add(Encoding.UTF8.GetString(payload));
        }

        private void btnEncenderLed_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("ESP8266ProfeCarlos", "L1", QoS.FireAndForget, false);
            }
        }

        private void btnApagarLed_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("ESP8266ProfeCarlos", "L0", QoS.FireAndForget, false);
            }
        }

        private void btnEstatusLed_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("ESP8266ProfeCarlos", "?L", QoS.FireAndForget, false);
            }
        }

        private void btnMoverServo_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("ESP8266ProfeCarlos", "S" + entAngulo.Text, QoS.FireAndForget, false);
            }
        }

        private void btnConsultarLuminosidad_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("ESP8266ProfeCarlos", "?F", QoS.FireAndForget, false);
            }
        }
    }
}
